<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\datasource\source
{
	use nuclio\core\
	{
		ClassManager,
		plugin\Plugin
	};
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	use nuclio\plugin\database\
	{
		exception\DatabaseException,
		common\CommonInterface as DBDriverInterface,
		common\CommonQueryInterface,
		common\CommonCursorInterface,
		common\DBRecord,
		common\DBVector,
		common\DBQuery,
		common\DBFields,
		common\DBCondition,
		common\DBOptions,
		orm\Model
	};
	
	<<factory>>
	class Source extends Plugin implements CommonQueryInterface
	{
		private string $driverName;
		public DBDriverInterface $driver;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Source
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(Map<string,string> $options)
		{
			parent::__construct();
			
			$driverName=$options->get('driver');
			if (is_null($driverName) || !is_string($driverName))
			{
				throw new DatabaseException('Unable to load driver for database. Driver must be provided as a string.');
			}
			$this->driverName=$driverName;
			$driverIntance=ProviderManager::request('database::'.$this->driverName,$options);
			if ($driverIntance instanceof DBDriverInterface)
			{
				$this->driver=$driverIntance;
			}
			else
			{
				throw new DatabaseException(sprintf('Unable to load driver for database. Driver "%s" is not available.',$this->driverName));
			}
		}
		
		public function getDriverName():string
		{
			return $this->driverName;
		}
		
		public function getDriver():DBDriverInterface
		{
			return $this->driver;
		}
		
		public function reconnect():void
		{
			//TODO
		}
		
		public function dropConnection():void
		{
			//TODO
		}
		
		public function find(string $target, DBQuery $query, DBOptions $options=Map{}):CommonCursorInterface
		{
			return $this->driver->find($target,$query,$options);
		}
		
		public function findOne(string $target, DBQuery $query, DBOptions $options=Map{}):?DBRecord
		{
			return $this->driver->findOne($target,$query,$options);
		}
		
		public function findById(string $target, mixed $id, DBOptions $options=Map{}):?DBRecord
		{
			return $this->driver->findById($target,$id,$options);
		}
		
		public function upsert(string $target, DBRecord $record, DBQuery $condition=Map{}, DBOptions $options=Map{}):DBRecord
		{
			return $this->driver->upsert($target,$record,$condition,$options);
		}
		
		public function delete(string $target, DBQuery $query, bool $justOne=false):bool
		{
			return $this->driver->delete($target,$query,$justOne);
		}
		
		public function distinct(string $target, string $field):Vector<string>
		{
			return $this->driver->distinct($target,$field);
		}
		
		public function listCollections():Vector<mixed>
		{
			return $this->driver->listCollections();
		}
		
		public function listCollectionFields(string $collection):Vector<Pair<string,sting>>
		{
			return $this->driver->listCollectionFields($collection);
		}
		
		public function buildColumn(string $name,Map<string,mixed> $annotations):string
		{
			return $this->driver->buildColumn($name,$annotations);
		}
		
		public function collectionExists(string $target):bool
		{
			return $this->driver->collectionExists($target);
		}
		
		public function columnExists(Model $model,string $column):bool
		{
			return $this->driver->columnExists($model,$column);
		}
		
		public function __call(string $method, array<mixed> $args):mixed
		{
			return call_user_func_array([$this->driver,$method],$args);
		}
	}
}
